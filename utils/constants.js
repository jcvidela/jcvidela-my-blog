export const name = "Juan Carlos";
export const surname = "Videla";
export const nickName = "Juanca";
export const fullName = `${name} ${surname}`;
export const currentPosition = "Frontend Developer"
export const siteTitle = `${fullName} | ${currentPosition}`;
