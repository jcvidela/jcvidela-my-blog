import styles from "./cssmodules/GridItem.module.css";
import { Card } from "react-bootstrap";

export default function GridItem({ title, description }) {
  return (
    <Card>
      <Card.Body>
        <h2>
          {title}
        </h2>
        <p className={styles.description}>{description}</p>
      </Card.Body>
    </Card>
  );
}
