import React from "react";
import Carousel from "react-elastic-carousel";
import Item from "../components/GridItem";
import ContactCard from "../components/ContactMe";
import utilStyles from "../styles/utils.module.css";
import styles from "./cssmodules/grid.module.css";

export default function Grid({ answers }) {
  return (
    <div className={utilStyles.responsiveGrid}>
      <Carousel className={styles.alignGrid}>
        {answers.map((card) => (
          <div key={card.id}>
            {card.title !== "Contact me" ? (
              <Item
                title={card.title}
                description={card.description}
                handleClick={() => showAnswer(card)}
              />
            ) : (
              <ContactCard />
            )}
          </div>
        ))}
      </Carousel>
    </div>
  );
}
