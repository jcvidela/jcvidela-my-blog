import React from "react";
import { linkElement, getCurrentAge } from "../helpers";

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [answer, setAnswer] = React.useState(null);
  const [answers] = React.useState([
    {
      id: "1",
      title: "My current self",
      description: (
        <span>
          I work as a Frontend developer at{" "}
          {linkElement("https://leverbox.com/ar/", "Leverbox")}, I usually use
          technologies like React.js, Redux and Node.js. I am interested in
          mobile technologies like React Native/Android. I am {getCurrentAge()}{" "}
          years old, In 2020 I started studying Computer Engineering at{" "}
          {linkElement("https://undav.edu.ar", "UNDAV")}.
        </span>
      ),
    },
    {
      id: "2",
      title: "¿Why did i learn to program?",
      description:
        "When I was 17 I was influenced by my 2 older brothers who work in technology, they suggested that I take an introductory course in programming. In the first course I learned the basics of frontend and later taking another backend course in which I decided that this was what I wanted to do..",
    },
    {
      id: "3",
      title: "How I see myself in future",
      description:
        "My goal is to be able to dedicate myself to the development of mobile applications with hybrid and native technologies.",
    },
    {
      id: "4",
      title: "Contact me",
      description: "",
    },
  ]);
  const [networks] = React.useState([
    {
      id: 1,
      url: "https://www.linkedin.com/in/jcvidela/",
      title: "LinkedIn",
      user: "jcvidela"
    },
    {
      id: 2,
      url: "https://github.com/jcvidela",
      title: "Github",
      user: "jcvidela"
    },
    {
      id: 3,
      url: "tel:+5491162064616",
      title: "Phone",
      user: "+5491162064616"
    },
    {
      id: 4,
      url: "mailto:juancarlos.videla00@gmail.com",
      title: "Email",
      user: "juancarlos.videla00"
    },
  ]);

  const state = {
    answers,
    networks,
  };

  return (
    <AppContext.Provider value={{ state }}>{children}</AppContext.Provider>
  );
};

export { AppContext as default, AppProvider as Provider };
